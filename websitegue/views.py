from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'home.html')

def profile(request):
    return render(request, 'profile.html')

def experiences(request):
    return render(request, 'experiences.html')

def skills(request):
    return render(request, 'skills.html')